import OpenDataApi as OD
import VkApi as VK
import time


# returns json, ready to load to Geo2Tag DB
def DataToLoad():
    odJson = OD.getDataset(29)
    vkCounts = []
    min_time = '01/12/2016'
    max_time = '16/12/2016'
    date_increment = 60 * 60 * 1  # every hour
    for i in odJson:
        if type(odJson) is int:
            continue
        vkJson = VK.getVkJson(i['row']['latitude'], i['row']['longitude'], i['row']['coverage'],
                              VK.datetotimestamp(min_time), VK.datetotimestamp(max_time), date_increment)
        vkCounts.append(vkJson['response']['count'])

    mean = Mean(vkCounts)

    pointsArray = []

    counter = 0
    for i in odJson:
        if type(odJson) is int:
            continue
        pointsArray.append(GetPoint(i, vkCounts[counter], mean))
        counter += 1

    return pointsArray


# returns average of numbers list
# don't use numpy just to minimize external dependencies
def Mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)


# returns 'json' object from point
def GetPointJson(jsonObj, postsNum, meanNum):
    obj = {}
    obj['number'] = jsonObj['row']['number']
    obj['name_object'] = jsonObj['row']['name_object']
    obj['address'] = jsonObj['row']['address']
    obj['district'] = jsonObj['row']['district']
    obj['name_wifi'] = jsonObj['row']['name_wifi']
    obj['coverage'] = jsonObj['row']['coverage']
    obj['status'] = jsonObj['row']['status']
    obj['note'] = jsonObj['row']['note']

    if postsNum >= 0.75 * meanNum:
        obj['popularity'] = 'high'
    elif postsNum >= 0.5 * meanNum:
        obj['popularity'] = 'above average'
    elif postsNum >= 0.25 * meanNum:
        obj['popularity'] = 'less than average'
    else:
        obj['popularity'] = 'low'

    return obj


def GetPoint(jsonObj, postsNum, meanNum):
    point = {'json': GetPointJson(jsonObj, postsNum, meanNum)}
    point['channel_id'] = "CHANNEL_ID"
    point['location'] = {
        "type": "Point",
        "coordinates": [
            float(jsonObj['row']['latitude']),
            float(jsonObj['row']['longitude'])
        ]
    }
    point['alt'] = 0
    point['bc'] = False
    point['date'] = time.time() #fix!
    return point

if __name__ == '__main__':
    print(DataToLoad())
