#43988c367f08778da456f41272a41e96dd98aed9
#wifi id = 29
import requests
import json
import time

#gets dataset info from data.gov.spb.ru
def getDatasetsInfo():
    headers = {'Authorization':'Token 43988c367f08778da456f41272a41e96dd98aed9'}
    result = requests.get('http://data.gov.spb.ru/api/v1/datasets/', headers=headers)


#gets dataset from data.gov.spb.ru
def getDataset(id):
    headers = {'Authorization': 'Token 43988c367f08778da456f41272a41e96dd98aed9'}
    result = requests.get('http://data.gov.spb.ru/api/v1/datasets/' + str(id) +
                          '/versions/latest/data/?per_page=100', headers=headers)
    result.encoding = 'utf-8'
    jsonObj = json.loads(result.text)
    return jsonObj

#prints json result to html file - its very comfortable to check result
def printToHtml(jsonObj):
    file_inst = open('od_go_spb' + time.strftime('%Y-%m-%d %H:%M:%S') + '.html', 'w')
    file_inst.write('<html>')
    file_inst.write('<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> </head>')
    for i in jsonObj:
        if type(i) is int:
            continue
        file_inst.write('<br>')
        file_inst.write('district: ' + i['row']['district'] + '><br>')
        file_inst.write('longitude: ' + i['row']['longitude'] + '><br>')
        file_inst.write('latitude: ' + i['row']['latitude'] + '><br>')
        file_inst.write('coverage: ' + i['row']['coverage'] + '><br>')
        file_inst.write('address: ' + i['row']['address'] + '><br>')
        file_inst.write('<br>')
    file_inst.write('</html>')
    file_inst.close()

if __name__ == '__main__':
    obj = getDataset(29)
    #printToHtml(obj)



