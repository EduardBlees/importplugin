import httplib  # httplib in python2.7
import json
import datetime
import time

#gets photos tagged in coordinates from vk
def get_vk(latitude, longitude, distance, min_timestamp, max_timestamp):
    get_request =  '/method/photos.search?lat=' + latitude
    get_request+= '&long=' + longitude
    get_request+= '&radius=' + distance
    get_request+= '&start_time=' + str(min_timestamp)
    get_request+= '&end_time=' + str(max_timestamp)
    get_request += '&v=5.60'
    connection = httplib.HTTPSConnection('api.vk.com', 443)
    connection.request('GET', get_request)
    return connection.getresponse().read()

#converts timestamp to date
def timestamptodate(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S') + ' UTC'

#converts date to timestamp
def datetotimestamp(date):
    return time.mktime(datetime.datetime.strptime(date, "%d/%m/%Y").timetuple())

#returns json from vk response
def getVkJson(location_latitude, location_longitude, distance, min_timestamp, max_timestamp, date_increment):
    print('Starting parse vkontakte..')
    print('GEO:',location_latitude,location_longitude)
    print('TIME: from',timestamptodate(min_timestamp),'to',timestamptodate(max_timestamp))
    response = get_vk(location_latitude, location_longitude, distance,
                      min_timestamp, max_timestamp).decode('utf-8')
    jsonObj = json.loads(response)
    print('count = ' + str(jsonObj['response']['count']))
    return jsonObj

#prints json result to html file - its very comfortable to check result
def printToHtml(jsonObj):
    file_inst = open('vk_' + location_latitude + location_longitude + '.html', 'w')
    file_inst.write('<html>')

    for local_i in jsonObj['response']['items']:
        if type(local_i) is int:
            continue
        file_inst.write('<br>')
        file_inst.write('<img src='+local_i['photo_604']+'><br>')
        file_inst.write(timestamptodate(int(local_i['date']))+'<br>')
        file_inst.write('http://vk.com/id'+str(local_i['owner_id'])+'<br>')
        file_inst.write('<br>')
    file_inst.write('</html>')
    file_inst.close()


location_latitude = '59.870335'
location_longitude = '30.307662'
distance = '100'
min_time = '01/12/2016'
max_time = '16/12/2016'
date_increment = 60 * 60 * 1  # every hour

if __name__ == '__main__':
    obj = getVkJson(location_latitude, location_longitude, distance,
                    datetotimestamp(min_time), datetotimestamp(max_time), date_increment)
    #printToHtml(obj)

