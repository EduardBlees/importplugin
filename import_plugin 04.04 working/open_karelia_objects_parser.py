import json
import DataHandler as DH
from open_data_objects_parser import OpenDataObjectsParser


class OpenKareliaObjectsParser(OpenDataObjectsParser):

    def parse(self):
        obj = DH.DataToLoad(self.data['days_to_load'], self.data['points_to_load'],self.data['method'])
        return obj
