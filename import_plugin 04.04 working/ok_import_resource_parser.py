from od_import_parser import OdImportParser, CHANNEL_NAME, OPEN_DATA_URL

class OKImportParser(OdImportParser):

    mandatoryFields = [
        CHANNEL_NAME,
        'days_to_load',
        'points_to_load',
        'method']
