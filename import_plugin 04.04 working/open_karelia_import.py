from perform_import_actions import performImportActions
from open_karelia_objects_loader import OpenKareliaObjectsLoader
from open_karelia_objects_parser import OpenKareliaObjectsParser
from open_karelia_object_to_point_translator import \
    OpenKareliaObjectToPointTranslator
from open_data_to_points_loader import OpenDataToPointsLoader


def openKareliaImport(
        _,
        channelName,
        days_to_load,
        points_to_load,
        method,
        serviceName):
    importDataDict = {
        'days_to_load': days_to_load,
        'points_to_load': points_to_load,
        'method': method}
    performImportActions(
        OpenKareliaObjectsLoader,
        OpenKareliaObjectsParser, #return all objects
        OpenKareliaObjectToPointTranslator, #return 1 point
        OpenDataToPointsLoader,
        channelName,
        importDataDict,
        importDataDict,
        serviceName)
    return None
