import threading
from job import Job


class ThreadJob(Job):

    def internalStart(self):
        thread = threading.Thread(
            target=self.backgroundFunction,
            args=(
                self,
                self.channelName,
                self.importDataDict.get('days_to_load'),
                self.importDataDict.get('points_to_load'),
                self.importDataDict.get('method'),
                self.serviceName,
            ))
        self.thread = thread
        thread.start()

    def describe(self):
        ancestorResult = Job.describe(self)
        ancestorResult['days_to_load'] = self.importDataDict.get('days_to_load')
        ancestorResult['points_to_load'] = self.importDataDict.get('points_to_load')
        ancestorResult['method'] = self.importDataDict.get('method')
        return ancestorResult
