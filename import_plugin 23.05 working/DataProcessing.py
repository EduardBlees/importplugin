import time
from cluster import KMeansClustering

# returns average of numbers list
# don't use numpy just to minimize external dependencies
def Mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def Median(numbers):
    sortedNumbers = sorted(numbers)
    numLen = len(sortedNumbers)
    index = (numLen - 1) // 2

    if (numLen % 2):
        return sortedNumbers[index]
    else:
        return (sortedNumbers[index] + sortedNumbers[index + 1]) / 2.0

def Cluster(numbers):
    return numbers

def GetData(odJson, vkArray, method, counter):
    result = []
    if method == 'mean':
        j = 0
        mean = Mean(vkArray)
        for i in odJson:
            if type(odJson) is int:
                continue
            result.append(GetPointMean(i, vkArray[j], mean))
            j += 1
            if (j == counter):
                break
        return result
    elif method == 'median':
        j = 0
        mean = Median(vkArray)
        for i in odJson:
            if type(odJson) is int:
                continue
            result.append(GetPointMedian(i, vkArray[j], mean))
            j += 1
            if j == counter:
                break
        return result
    elif method == 'cluster':
        clusters = GetClusters(vkArray)
        j = 0
        for i in odJson:
            if type(odJson) is int:
                continue
            result.append(GetPointCluster(i, vkArray[j], clusters))
            j += 1
            if j == counter:
                break
        return result
    else:
        return result

# returns 'json' object from point
def GetJsonMean(jsonObj, postsNum, meanNum):
    obj = {}
    obj['number'] = jsonObj['row']['number']
    obj['name_object'] = unicode(jsonObj['row']['name_object'])
    obj['address'] = unicode(jsonObj['row']['address'])
    obj['district'] = unicode(jsonObj['row']['district'])
    obj['name_wifi'] = jsonObj['row']['name_wifi']
    obj['coverage'] = jsonObj['row']['coverage']
    obj['status'] = unicode(jsonObj['row']['status'])
    obj['note'] = jsonObj['row']['note']

    if postsNum >= 1.5 * meanNum:
        obj['popularity'] = 'high'
    elif postsNum >= meanNum:
        obj['popularity'] = 'above average'
    elif postsNum >= 0.5 * meanNum:
        obj['popularity'] = 'less than average'
    else:
        obj['popularity'] = 'low'

    return obj


def GetPointMean(jsonObj, postsNum, meanNum):
    point = {'json': GetJsonMean(jsonObj, postsNum, meanNum)}
    point['channel_id'] = "CHANNEL_ID"
    point['location'] = {
        "type": "Point",
        "coordinates": [
            float(jsonObj['row']['latitude']),
            float(jsonObj['row']['longitude'])
        ]
    }
    point['alt'] = 0
    point['bc'] = False
    point['date'] = time.time() #fix!
    return point

# returns 'json' object from point
def GetJsonMedian(jsonObj, postsNum, medianNum):
    obj = {}
    obj['number'] = jsonObj['row']['number']
    obj['name_object'] = unicode(jsonObj['row']['name_object'])
    obj['address'] = unicode(jsonObj['row']['address'])
    obj['district'] = unicode(jsonObj['row']['district'])
    obj['name_wifi'] = jsonObj['row']['name_wifi']
    obj['coverage'] = jsonObj['row']['coverage']
    obj['status'] = unicode(jsonObj['row']['status'])
    obj['note'] = jsonObj['row']['note']

    if postsNum >= 1.5 * medianNum:
        obj['popularity'] = 'high'
    elif postsNum >= medianNum:
        obj['popularity'] = 'above median'
    elif postsNum >= 0.5 * medianNum:
        obj['popularity'] = 'less than median'
    else:
        obj['popularity'] = 'low'

    return obj


def GetPointMedian(jsonObj, postsNum, medianNum):
    point = {'json': GetJsonMedian(jsonObj, postsNum, medianNum)}
    point['channel_id'] = "CHANNEL_ID"
    point['location'] = {
        "type": "Point",
        "coordinates": [
            float(jsonObj['row']['latitude']),
            float(jsonObj['row']['longitude'])
        ]
    }
    point['alt'] = 0
    point['bc'] = False
    point['date'] = time.time() #fix!
    return point

def GetClusters(array):
    points = []
    count = len(array)
    if count < 3:
        return []
    for i in range(0, count):
        points.append((array[i], 0))

    cl = KMeansClustering(points)
    clusters = cl.getclusters(2)

    return clusters

# returns 'json' object from point
def GetJsonCluster(jsonObj, postsNum, cluster):
    obj = {}
    obj['number'] = jsonObj['row']['number']
    obj['name_object'] = unicode(jsonObj['row']['name_object'])
    obj['address'] = unicode(jsonObj['row']['address'])
    obj['district'] = unicode(jsonObj['row']['district'])
    obj['name_wifi'] = jsonObj['row']['name_wifi']
    obj['coverage'] = jsonObj['row']['coverage']
    obj['status'] = unicode(jsonObj['row']['status'])
    obj['note'] = jsonObj['row']['note']

    clusterIndex = 2
    #it's a trap!
    for i in cluster[0]:
        count = len(i)
        for j in range(0, count-1):
            if i[0] == postsNum:
                clusterIndex = 0
                break
    for i in cluster[1]:
        count = len(i)
        for j in range(0, count-1):
            if i[0] == postsNum:
                clusterIndex = 1
                break

    obj['popularity'] = 'cluster_' + str(clusterIndex)

    return obj


def GetPointCluster(jsonObj, postsNum, cluster):
    point = {'json': GetJsonCluster(jsonObj, postsNum, cluster)}
    point['channel_id'] = "CHANNEL_ID"
    point['location'] = {
        "type": "Point",
        "coordinates": [
            float(jsonObj['row']['latitude']),
            float(jsonObj['row']['longitude'])
        ]
    }
    point['alt'] = 0
    point['bc'] = False
    point['date'] = time.time() #fix!
    return point