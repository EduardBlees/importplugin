from datetime import datetime
from open_data_object_to_point_translator import \
    OpenDataToPointTranslator


class OpenKareliaObjectToPointTranslator(
        OpenDataToPointTranslator):

    def __init__(
            self,
            importDataDict,
            objectRepresentation,
            version,
            importSource,
            channelId):
        super(
            OpenKareliaObjectToPointTranslator,
            self).__init__(
            importDataDict,
            objectRepresentation,
            version,
            importSource,
            channelId)

    def getPointJson(self):
        obj = {}
        obj['name'] = 'name'
        obj['image_url'] = 'image'
        obj['source_url'] = 'source'
        obj['version'] = 'ver'
        obj['import_source'] = 'isource'
        return obj

    def getPoint(self):
        point = {'json': self.getPointJson()}
        point['channel_id'] = self.channelId
        self.objectRepresentation['channel_id'] = self.channelId
        return self.objectRepresentation

