import OpenDataApi as OD
import VkApi as VK
import time
import datetime
import DataProcessing as DP


# returns json, ready to load to Geo2Tag DB
def DataToLoad(daysToLoad, pointsToLoad, method):
    start_time = time.time()
    odJson = OD.getDataset(29)
    od_time = time.time() -  start_time
    vkCounts = []
    if (int(daysToLoad) < 1):
        daysToLoad = 1
    if (int(pointsToLoad) < 1):
        pointsToLoad = 5
    max_time = time.strftime("%d/%m/%Y")
    min_time = datetime.datetime.now() - datetime.timedelta(days=int(daysToLoad))
    min_time = min_time.strftime("%d/%m/%Y")
    date_increment = 60 * 60 * 1  # every hour
    j = 0
    for i in odJson:
        if type(odJson) is int:
            continue
        vkJson = VK.getVkJson(i['row']['latitude'], i['row']['longitude'], i['row']['coverage'],
                              VK.datetotimestamp(min_time), VK.datetotimestamp(max_time), date_increment)
        vkCounts.append(vkJson['response']['count'])
        j += 1
        if (j == int(pointsToLoad)):
            break

    odvk_time = time.time() - start_time
    pointsArray = DP.GetData(odJson, vkCounts, method, int(pointsToLoad))

    work_time = time.time() - start_time

    filename = 'file_d' + str(daysToLoad) + '_p' + str(pointsToLoad) + '.txt'
    f = open(filename, "w")
    f.write('od_odvk_full: ' + str(od_time) + '\t' + str(odvk_time) + '\t' + str(work_time))

    return pointsArray

# returns 'json' object from point
def GetPointJson(jsonObj, postsNum, meanNum):
    obj = {}
    obj['number'] = jsonObj['row']['number']
    obj['name_object'] = unicode(jsonObj['row']['name_object'])
    obj['address'] = unicode(jsonObj['row']['address'])
    obj['district'] = unicode(jsonObj['row']['district'])
    obj['name_wifi'] = jsonObj['row']['name_wifi']
    obj['coverage'] = jsonObj['row']['coverage']
    obj['status'] = unicode(jsonObj['row']['status'])
    obj['note'] = jsonObj['row']['note']

    if postsNum >= 1.5 * meanNum:
        obj['popularity'] = 'high'
    elif postsNum >= meanNum:
        obj['popularity'] = 'above average'
    elif postsNum >= 0.5 * meanNum:
        obj['popularity'] = 'less than average'
    else:
        obj['popularity'] = 'low'

    return obj


def GetPoint(jsonObj, postsNum, meanNum):
    point = {'json': GetPointJson(jsonObj, postsNum, meanNum)}
    point['channel_id'] = "CHANNEL_ID"
    point['location'] = {
        "type": "Point",
        "coordinates": [
            float(jsonObj['row']['latitude']),
            float(jsonObj['row']['longitude'])
        ]
    }
    point['alt'] = 0
    point['bc'] = False
    point['date'] = time.time()
    return point

if __name__ == '__main__':
    print(DataToLoad())
